<?php
$url = 'http://opdracht.loc/vacatureservice/vacatures.xml';
$allowed_tags = '<a><br><ul><ol><li><b><i>';

// Laad XML
$context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
$file_content = file_get_contents($url, false, $context);

// Veld functieomschrijving kan HTML bevatten, CDATA toevoegen aan xml
$patterns = array(
    '/<functie_omschrijving>/',
    '/<\/functie_omschrijving>/'
);
$replacements = array(
    '<functie_omschrijving><![CDATA[',
    ']]></functie_omschrijving>'

);
$file_content = preg_replace($patterns, $replacements, $file_content);
$vacatures_xml = simplexml_load_string($file_content);

// Maak nieuwe array met opgeschoonde velden
$output = array();
foreach ($vacatures_xml as $vacature) {
    $fields = array();
    foreach ($vacature->children() as $field_name => $field_content) {
        $field_name = trim(strip_tags($field_name));
        $stripped_content = trim(strip_tags($field_content, $allowed_tags));
        $fields[$field_name] = $stripped_content;
    }
    $output[] = $fields;
}

// Reduceer het aantal resultaten
$page = !isset($_GET['page']) ? 1 : $_GET['page'];
$pageLimit = !isset($_GET['pageLimit']) ? 5 : $_GET['pageLimit'];
$offset = ($page - 1) * $pageLimit; // offset
$total_items = count($raw_data); // total items
$total_pages = ceil($total_items / $pageLimit);
$output = array_splice($output, $offset, $pageLimit);

// Print output als JSON
header('Content-Type: application/json');
$output = json_encode(array('vacatures' => $output));
print($output);
