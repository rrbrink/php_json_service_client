Bevat een JSON webservice (PHP) en een Javascript client om vacatures weer te geven.

Uitleg mappen/configuratie:

De map 'vacatureservice' bevat een JSON webservice die gebruikt wordt om de vacatures te tonen. Deze webservice is om de aangeleverde XML gemaakt.

De map 'website' is, zoals de naam al doet vermoeden, de website waarin de vacatures weergegeven worden.

Uitgangspunt in de code is dat deze draait op opdracht.loc (opnemen in hosts-file), kan aangepast worden door aanpassen van:
- variable $url in vacatureservice/vacatures.php
- variable $serviceUrl in website/js/scripts.js
