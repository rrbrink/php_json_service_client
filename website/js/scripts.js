var page = 1;
var itemsPerPage = 40;
var serviceUrl = 'http://opdracht.loc/vacatureservice/vacatures.php';
var interval = null;
var jobsList = '#container .jobslist';

// Toon aantal resultaten
var loadResultSet = function () {
    $.getJSON({
        url: serviceUrl + '?pageLimit=' + itemsPerPage + '&page=' + page
    }).done(function (data) {
        var resultLength = data.vacatures.length;
        if (resultLength > 0) {
            for (var i = 0; i < resultLength; i++) {
                renderJob(data.vacatures[i]);
            }
        } else {
            $('<p>Er zijn niet meer vacatures</p>').appendTo($(jobsList));
            clearInterval(interval);
        }
    }).fail(function () {
        $('<p>Er is momenteel een storing, probeert u het later nogmaals</p>').appendTo(jobsList);
    });
    page++;
};

loadResultSet();

// Render html voor 1 resultaat
function renderJob(job) {
    var jobTitle = job.titel;
    var jobDescription = job.functie_omschrijving;
    var jobLink = job.sollicitatie_link;
    var listItem = $('<div class="item"></div>').appendTo($(jobsList));
    var headerTitle = $('<a href="#"><h2>' + jobTitle + '</h2></a>');
    var jobDetails = $('<div class="jobdetails"></div>');

    headerTitle.appendTo(listItem);
    headerTitle.click(headerClickHandler);
    jobDetails.appendTo(listItem);
    jobDetails.hide();
    $('<div class="description">' + jobDescription + '</div>').appendTo(jobDetails);
    $('<a href="' + jobLink + '" class="link belowcontent" target="_blank">Sollicitatie link (opent in nieuw venster)</a>').appendTo(jobDetails);
}

// Click handler voor elk resultaat
function headerClickHandler(e) {
    e.preventDefault();
    var $this = $(this).parent().find('.jobdetails');
    $(jobsList + ' .jobdetails').not($this).hide();
    $this.toggle();
}

// interval scroll naar einde pagina detectie
interval = setInterval(function () {
    var targetPercentage = 99;
    var scrollTo = $(window).scrollTop();
    var docHeight = $(document).height();
    var windowHeight = $(window).height();
    var scrollPercent = (scrollTo / (docHeight - windowHeight)) * 100;
    var scrollPercent = scrollPercent.toFixed(1);
    if (scrollPercent > targetPercentage) {
        loadResultSet();
    }
}, 1000);